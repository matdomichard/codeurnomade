export interface JobOfferCounts {
  [key: string]: number;
}

export interface LanguagePercentage {
  language: string;
  percentage: number;
}
